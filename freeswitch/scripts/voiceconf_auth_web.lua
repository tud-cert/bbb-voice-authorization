-- verify if user in SIP call is in meeting that matches the called number
-- call in dialplan like this:
-- <action application="lua" data="voiceconf_auth.lua ${destination_number} ${caller_id_number}"/>
-- it will set bbb_authorized=true or false

-- depends on sha1
-- depends on xml2lua
-- deoends on httpclient

-- fix for lua not finding required modules
package.path = package.path .. ";" .. [[/usr/share/lua/5.2/?.lua]] .. ";" .. [[/usr/local/share/lua/5.2/?.lua]]

function logInfo(msg)
   freeswitch.consoleLog("info",msg)
   -- print("INFO " .. msg)
end
function logDebug(msg)
   freeswitch.consoleLog("debug",msg)
   -- print("DEBUG " .. msg)
end
function logError(msg)
   freeswitch.consoleLog("debug",msg)
   -- rint("ERROR " .. msg)
end


xml2lua = require("xml2lua")
http = require('httpclient').new()
sha = require("sha1")

logDebug("voiceconf_auth: start")

function get_meetings_xml()
   baseUrl = "http://127.0.0.1:8090/bigbluebutton/api/"
   secret="SECRET_TEMPLATE"
   checksum = sha.sha1("getMeetings" .. secret)
   getMeetingsUrl = baseUrl .. "getMeetings" .. "?checksum=" .. checksum
   response = http:get(getMeetingsUrl)

   if not(response) or not(response.code == 200) then
      logError("voiceconf_auth: Error during API call")
      return false
   else
      return response.body
   end
end

function parse_meetings_xml(xml)
   handler = require("xmlhandler.tree")
   parser = xml2lua.parser(handler)
   parser:parse(xml)
   return handler.root.response.meetings.meeting
end

function is_user_in_meeting(caller, meeting)
   if meeting.attendees.attendee[1] then
      for j,u in pairs(meeting.attendees.attendee) do
         logDebug("check userID=".. u.userID)
         if u.userID == caller then
            -- got a match, allow call
            return true
         end
      end
   else
      if meeting.attendees.attendee.userID == caller then
         return true
      end
   end
   return false
end

function is_authorized(caller, tel, meetings)
   -- loop through meetings and check which meetings has a voiceBridge equal to "destination_number"
   if meetings[1] then      
      for i,m in pairs(meetings) do
         logDebug("check voiceBridge=".. m.voiceBridge)
         if tel == m.voiceBridge then
            -- got match, now look for an attendee with a userID equal to caller
            if is_user_in_meeting(caller, m) then
               return true
            end
         end
      end
   else
      -- single meeting, no list
      logDebug("check single voiceBridge=".. meetings.voiceBridge)
      if tel == meetings.voiceBridge then
         -- got match, now look for an attendee with a userID equal to caller
         if is_user_in_meeting(caller, meetings) then
            return true
         end
      end
   end
   return false
end

-- MAIN --
dest_no = argv[1]
caller_no = argv[2]

logDebug("voiceconf_auth: caller_no:" .. caller_no .. " dest_no:" .. dest_no)

-- remove leading "echo" from destination
dest = string.gsub(dest_no, "echo", "")
-- remove audioSessionNumber, bbbID and name from caller_number
userid = caller_no:gsub("_[0-9]+-bbbID-.*","")

logInfo("voiceconf_auth: cleaned up. caller_no:" .. userid .. " dest_no:" .. dest)

xml = get_meetings_xml()
meetings = parse_meetings_xml(xml)

if is_authorized(userid, dest, meetings) then
   logInfo("voiceconf_auth: AUTHORIZED call from " .. caller_no .. " to " .. dest_no)
   session:execute("set", "bbb_authorized=true")
else
   logInfo("voiceonf_auth: UNAUTHORIZED call from " .. caller_no .. " to " .. dest_no .. ".")
   session:execute("set", "bbb_authorized=false")
end
