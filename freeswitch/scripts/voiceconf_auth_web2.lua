-- verify if user in SIP call is in meeting that matches the called number
-- call in dialplan like this:
-- <action application="lua" data="voiceconf_auth.lua ${destination_number} ${caller_id_number}"/>
-- it will set bbb_authorized=true or false

-- deoends on httpclient

-- fix for lua not finding required modules
package.path = package.path .. ";" .. [[/usr/share/lua/5.2/?.lua]] .. ";" .. [[/usr/local/share/lua/5.2/?.lua]]

function logInfo(msg)
   freeswitch.consoleLog("info",msg .. "\n")
   -- print("INFO " .. msg)
end
function logDebug(msg)
   freeswitch.consoleLog("debug",msg .. "\n")
   -- print("DEBUG " .. msg)
end
function logError(msg)
   freeswitch.consoleLog("debug",msg .. "\n")
   -- print("ERROR " .. msg)
end

socket_ok, http = pcall(require, "socket.http")
luasec_ok, https = pcall(require, "ssl.https")

logDebug("voiceconf_auth: start")


function check_user_in_meeting(userId, voiceBridge)
   local baseUrl = bbb_base_url .. "/bigbluebutton/connection/checkVoiceBridgeAuthorization"
   local check_auth_url = baseUrl .. "?userId=" .. userId .. "&voiceBridge=" .. voiceBridge
   -- logDebug("check url: " .. check_auth_url)
   if baseUrl:find("https://") then
      proto = https
   else
      proto = http
   end
   
   local res, status, headers, err = proto.request {
      url = check_auth_url,
      method = 'GET',
      redirect = false,
      -- TLS parameters, adjust if Required
      protocol = "tlsv1_2",
      -- options  = {"all", "no_sslv2", "no_sslv3", "no_tlsv1"},
      verify   = "none",
   }
   
   if not(status) or not(status == 200) then
      return false
   else
      return true
   end
end



-- MAIN --
dest_no = argv[1]
caller_no = argv[2]

logDebug("voiceconf_auth: caller_no:" .. caller_no .. " dest_no:" .. dest_no)

bbb_base_url = session:getVariable("bbb_base_url")
if bbb_base_url then
   logDebug("got bbb_base_url: " .. bbb_base_url)
else
   logDebug("No bbb_base_url, setting localhost")
   bbb_base_url = "http://localhost:8090/"
end

-- remove leading "echo" from destination
dest = string.gsub(dest_no, "echo", "")
-- remove audioSessionNumber, bbbID and name from caller_number
userid = caller_no:gsub("_[0-9]+-bbbID-.*","")

logInfo("voiceconf_auth: cleaned up. caller_no:" .. userid .. " dest_no:" .. dest)

if check_user_in_meeting(userid, dest) then
   logInfo("voiceconf_auth: AUTHORIZED call from " .. caller_no .. " to " .. dest_no)
   session:execute("set", "bbb_authorized=true")
else
   logInfo("voiceonf_auth: UNAUTHORIZED call from " .. caller_no .. " to " .. dest_no .. ".")
   session:execute("set", "bbb_authorized=false")
end
