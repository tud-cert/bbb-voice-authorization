-- verify if user in SIP call is in meeting that matches the called number
-- call in dialplan like this:
-- <action application="lua" data="voiceconf_auth.lua ${destination_number} ${caller_id_number}"/>
-- it will set bbb_authorized=true or false

-- fix for lua not finding required modules
package.path = package.path .. ";" .. [[/usr/share/lua/5.2/?.lua]] .. ";" .. [[/usr/local/share/lua/5.2/?.lua]]

function logInfo(msg)
   freeswitch.consoleLog("info",msg)
end
function logDebug(msg)
   freeswitch.consoleLog("debug",msg)
end

logDebug("voiceconf_auth: start")

-- caller_name  = session:getVariable("caller_id_name");
-- caller_no    = session:getVariable("caller_id_number");
-- dest_no      = session:getVariable("destination_number");
dest_no = argv[1]
caller_no = argv[2]


mongoclient  = require("mongorover.MongoClient")
client       = mongoclient.new("mongodb://127.0.1.1/")
db           = client:getDatabase("meteor")
meetings     = db:getCollection("meetings")
users        = db:getCollection("users")


logDebug("voiceconf_auth: caller_no:" .. caller_no .. " dest_no:" .. dest_no)

-- remove leading "echo" from destination
dest = string.gsub(dest_no, "echo", "")
-- remove audioSessionNumber, bbbID and name from caller_number
userid = caller_no:gsub("_[0-9]+-bbbID-.*","")

logDebug("voiceconf_auth: cleaned up. caller_no:" .. userid .. " dest_no:" .. dest)

authorized = false
-- find_one doesn't work, so I'm using find in a loop (there will be only 1 matching entry)
for u in users:find({userId = userid})
do
   meetingid = u["meetingId"]
   for m in meetings:find({meetingId = meetingid})
   do
      tel = m["voiceProp"]["telVoice"]
      if tel == dest
      then
         -- got a match, allow call
         authorized = true
         logInfo("voiceconf_auth: AUTHORIZED call from " .. caller_no .. " to " .. dest_no)
         session:execute("set", "bbb_authorized=true")
      end
   end
end

if not authorized
then
   logInfo("voiceonf_auth: UNAUTHORIZED call from " .. caller_no .. " to " .. dest_no .. ".")
   session:execute("set", "bbb_authorized=false")
end
