#!/bin/bash
set -e

usage() {
    cat <<EOF
Usage: $0

This script will install fixes to secure access to BBB voice
conferences only if the caller is in listed as an attendee of the
BBB-meeting.

EOF
}

printErr () {
    echo "Error occured (${?}). Exiting..."
    exit
}

trap printErr ERR


if [ $# -gt 0 -a "$1" = "-h" -o "$1" = "--help" ]
then
    usage
    exit
fi

if [ $# = 0]
then
    usage
    print "Press [RETURN] to continue. Or CTRL-C to abort."
    read x
fi

umask 022
DIR=$(readlink -e "$(dirname "$0")")

echo "### installing lua dependencies"
apt-get install lua-socket lua-sec

echo "### deploy freeSWITCH config and scripts"
install -m755 $DIR/freeswitch/scripts/voiceconf_auth_web2.lua /opt/freeswitch/share/freeswitch/scripts/voiceconf_auth_web2.lua
rsync -r --chmod=D0755,F0644 $DIR/freeswitch/sip_profiles/ /opt/freeswitch/conf/sip_profiles/
rsync -r --chmod=D0755,F0644 $DIR/freeswitch/dialplan/ /opt/freeswitch/conf/dialplan/

echo "### deploy changed bbb-web classes"
rsync -r --chmod=D0755,F0644 $DIR/dist/WEB-INF/ /usr/share/bbb-web/WEB-INF/

pushd /
echo "### patch bbb-webrtc-sfu AudioManager"
patch -b -p0 < $DIR/patches/AudioManager-2.2.0.patch

echo "### patch bbb-conf script"
patch -b -p1 < $DIR/patches/bbb-conf.patch

popd

# Read config variables, taken from bbb-conf
if [ -f /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties ]; then
    SERVLET_DIR=/usr/share/bbb-web
else
    SERVLET_DIR=/var/lib/tomcat7/webapps/bigbluebutton
fi
PROTOCOL=http
if [ -f $SERVLET_DIR/WEB-INF/classes/bigbluebutton.properties ]; then
    SERVER_URL=$(cat $SERVLET_DIR/WEB-INF/classes/bigbluebutton.properties | sed -n '/^bigbluebutton.web.serverURL/{s/.*\///;p}')
    if cat $SERVLET_DIR/WEB-INF/classes/bigbluebutton.properties | grep bigbluebutton.web.serverURL | grep -q https; then
        PROTOCOL=https
    fi
fi

echo "### add $SERVER_URL as bbb_base_url to freeswitch vars"
FREESWITCH_VARS=/opt/freeswitch/conf/vars.xml
if grep -q 'data="bbb_base_url' $FREESWITCH_VARS
then
    sed -i -e "s|data=\"bbb_base_url=[^\"]\+\"/>|data=\"bbb_base_url=$SERVER_URL\"/>|" $FREESWITCH_VARS
else
    sed -i -e "s|</include>|  <X-PRE-PROCESS cmd=\"set\" data=\"bbb_base_url=$SERVER_URL\"/>\\n</include>|" $FREESWITCH_VARS
fi

echo "### add $SERVER_URL as bbbUrl in $KURENTO_CONFIG"
KURENTO_CONFIG=/usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml
if [ -f $KURENTO_CONFIG ]; then
    yq w -i $KURENTO_CONFIG bbbUrl "$SERVER_URL"
fi

echo "### configure bbb-webrtc-sfu to connect to freeswitch on port 5062"
yq write -i /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml freeswitch.port 5062

echo "### restart freeswitch, BBB-web, BBB-webrtc-sfu"
systemctl restart freeswitch
systemctl restart bbb-web
systemctl restart bbb-webrtc-sfu
