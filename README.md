# Bigbluebutton voice / audio-conference authorization #

This repository contains code and patches to implement audio
conference authorization in BBB.

## Background / Motivation

Up until now (BBB 2.2.25), only a very simple
authentication/authorization scheme is available. Websocket
connections to FreeSWITCH and BBB-webrtc-sfu are checked for a valid
sessionToken, but there is no access control for individual
conferences.

This means an attacker with access to a BBB meeting could connect to
all voice conferences on the same server by enumerating conference
numbers (5 digits by default).

Audio conferences in BBB are implemented by FreeSWITCH and initiated
via SIP over HTTP. Listen-only participants don't connect to
FreeSWITCH directly, but to `bbb-webrtc-sfu`, which tells `kurento` to
join a FreeSWITCH conference and sets up connections between web
clients an kurento. Kurento is said to scale better than FreeSWITCH,
that's why this approach is chosen in BBB. See [BBB's architecture
page](https://docs.bigbluebutton.org/2.2/architecture.html) for
details.


## Implementation

When joining a BBB conference, each client is assigned a random
userId, and associated to a meeting. The client is then told their
userID, a sessionToken, the meeting id, and the voiceBridge number
(amongst others). This information is used to decide wether a client
(with a given userId may access a voice conference (= voiceBridge).

### BBB-Web

The original simple authentication/authorization was implemented in
`org.bigbluebutton.web.controllers.ConnectionController`. This class
was extended with methods that check wether a `userId` (or `sessionToken`)
may access a given `voiceBridge`.

The patch is in [bbb-web-2.2.25.patch](patches/bbb-web-2.2.25.patch). You'll
have to apply this patch to bigbluebutton-web, then rebuild the code,
and redeploy (at least the changed classes `ConnectionController`,
`UrlMappings`, and any contained classes `ConnectionController$_*`,
`UrlMappings$_*` - these are also available in [dist/WEB-INF](dist/WEB-INF). 

If you use multiple freeSWITCH/bbb-webrtc-sfu instances, or freeSWITCH
runs on a different hose, then these new URL endpoints must be made
accessible through nginx. See [web.nginx.patch](patches/web.nginx.patch).

This change is the base for securing bbb-webrtc-sfu and freeSWITCH
(although, there's an alternative fix for freeSWITCH available that
doesn't need any changes to other components).

### FreeSWITCH

FreeSWITCH allows custom actions during call setup (so called dialplan
actions). One of the scripts linked below, must be called by the
"public" dialplan, ie. by incoming calls from web clients. The script
checks if the caller is authorized to call a given `voiceBridge`
number. If it does, `bbb_authorized` is set to `true` and the call
continues. If not, `bbb_authorized` ist set to `false` and the call
will be ended.

- [voiceconf_auth_web2.lua](freeswitch/scripts/voiceconf_auth_web2.lua) -
  diaplan script that talks to BBB-Web's authorization
  endpoint. Doesn't need any special permissions/API secrets to
  work. It depends on the `httpclient` lua module. The lua script
  expects destination and caller as arguments (see dialplan config
  below).
- [bbb_webrtc.xml](freeswitch/dialplan/public/bbb_webrtc.xml) /
  [bbb_sip.xml](freeswitch/dialplan/public/bbb_sip.xml) - public
  dialplan configuration files.
- `kurento`/`bbb-webrtc-sfu` cannot access secured freeSWITCH
  conferences any more. To restore access, a separate SIP profile and
  dialplan for bbb-webrtc-sfu is created. This allows unauthorized
  access to freeSWITCH from bbb-webrtc-sfu, so make sure to secure
  this component as well.
  - [dialplan/bbb-webrtc-sfu.xml](freeswitch/dialplan/bbb-webrtc-sfu.xml)
    dialplan that sets `bbb_authorized=true` and transfers to the
    default/internal dialplan
  - [sip_profiles/bbb-webrtc-sfu.xml](freeswitch/sip_profiles/bbb-webrtc-sfu.xml)
    SIP profile accessible on a different port (5062) that uses the
    `bbb-webrtc-sfu` dialplan
  - `bbb-webrtc-sfu` must be configured to access this SIP port:
    `yq write -i /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml freeswitch.port 5062`

Please note: To install these lua modules, you need lua5.2, and
liblue5.2-dev. If you use the install script (`install.sh`), they
should be installed automatically.

#### alternative FreeSWITCH dialplan scripts

The following dialplan scripts secure BBB w/o changes to
Bigbluebutton-web and bbb-webrtc-sfu. Please note, this will
effectively disable the listen-only audio through
bbb-webrtc-sfu/kurento, as the authorization scripts will not allow
connections from the sfu. Luckily, the BBB html5 client will fall-back
to a regular SIP-through-websocket connection (in listen-only mode),
and listen-only audio will still work.

- [voiceconf_auth_web.lua](freeswitch/scripts/voiceconf_auth_web.lua) -
  talks to BBBs web API. Needs a BBB API secret and access to the Web
  API (URL and secret configurable in the script). In addition, the
  lua modules `sha1`, `httpclient` and `xml2lua` are required.
- [voiceconf_auth.lua](freeswitch/scripts/voiceconf_auth.lua)` - talks
  to mongodb. So it must be able to access mongodb from freeswitch and
  needs the `mongorover` lua module.

### BBB-webrtc-sfu

Within `bbb-webrtc-sfu`, setup of client's audio sessions is
implemented in `AudioManager.js`. Upon receiving a `start` message,
this component checks if the client (`userId`) is authorized to access
a `voiceBridge` and either aborts the connection or resumes session
setup.

The patch for version 2.2.0 is in
[AudioManager-2.2.0.patch](patches/AudioManager-2.2.0.patch). Since
this is "only" javascript, this patch could be applied on a live
system, no rebuild is necessary (but please note that this depends on
the `BBB-Web` changes described above).

## Installation

These fixes can be installed on a system with BBB version 2.2.25 with
the install script [install.sh](install.sh).
